# Spinformation

This is a first attempt at using machine learning to simplify measurements required for EFT limits.

## notebooks

Easiest if used within the Jupyter environment.

```bash
# clone this repo, then...
cd spinformation
pipenv install
pipenv run jupyter lab
```

## scripts

All python modules and scripts will be store here.
To run a notebook, for example:
```bash
jupyter notebook notebooks/KIN_NW_reg_01.ipynb
```

## data

The contents of this folder are skipped by the gitignore, but all data for the project will be assumed to be store here.

The data handling will be handled with Uproot from root to python. [Repo](https://github.com/scikit-hep/uproot)

## installation notes

pipenv was installed on Ubuntu 18.04.3 LTS with the following

```bash
sudo apt install python-pip
sudo -H pip install pipenv
```

PyTorch was installed for MacOS with the following command

```bash
pipenv install torch torchvision
```

Jupyter launching wrong application in pyenv (in Ubuntu 16.04.6) was solved as described in [this stackoverflow reply](https://stackoverflow.com/questions/58180711/launching-jupyter-notebook-from-terminal-starts-signal-desktop-messenger-as-well/58572811#58572811)

### Links, references, and inspiration

[Autoencoder Based Residual Deep Networks for Robust Regression Prediction and Spatiotemporal Estimation](https://arxiv.org/pdf/1812.11262.pdf)

[Variational AutoEncoder For Regression: Application to Brain Aging Analysis](https://arxiv.org/pdf/1904.05948.pdf)

[Network model tutorials](https://github.com/MorvanZhou/PyTorch-Tutorial)

