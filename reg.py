#!/usr/bin/env python3

import os
#os.environ["KERAS_BACKEND"] = "plaidml.keras.backend"

import uproot
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D
from keras.models import Sequential
from keras.layers import Dense
from keras.models import Model
from keras.layers import Dense, Input, Dropout, MaxPooling2D, Flatten
from keras import regularizers
from keras.layers.merge import concatenate
from sklearn.preprocessing import RobustScaler
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from scipy.stats import ks_2samp

from src import ml_analysis_plotting


def  get_input_arrays(tree, features_list):
    array = np.transpose(tree.arrays(features_list, outputtype=tuple))
    print(array.shape)
    print(array[0])
    return array
tree = uproot.open('data/full_spin_03_small.root')[b'spin_truth;1']
tree.keys()

features_list = [b'reco_t_pt',
 b'reco_t_eta',
 b'reco_t_phi',
 b'reco_tbar_pt',
 b'reco_tbar_eta',
 b'reco_tbar_phi',
 b'reco_lep_1_pt',
 b'reco_lep_1_eta',
 b'reco_lep_1_phi',
 b'reco_lep_2_pt',
 b'reco_lep_2_eta',
 b'reco_lep_2_phi']

spin_target = tree.array(b'kk')#[np.where(spin_target !=  -10.)[0]]
#pt_cut = np.where(spin_target/1000.0 < 300.)[0]
#spin_target = spin_target[pt_cut]
h,b,_=plt.hist(spin_target, bins=25)
plt.savefig('temp_kk_truth.png')
print(h)
print(b)
