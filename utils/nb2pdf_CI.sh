cd src
for file in ../notebooks/*.ipynb
do
    file=${file##*/}
    file=${file%.*}
    pipenv run python run_notebook.py ../notebooks/"$file".ipynb;
    pipenv run jupyter nbconvert ../notebooks/"$file"_out.ipynb --to pdf
    rm -rf ../notebooks/"$file"_out.ipynb
    mv ../notebooks/"$file"_out.ipynb ../doc
done
cd ..