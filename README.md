## Jupyter notebooks

Easiest if used within the Jupyter environment.

# Initial set up

## miniconda3 on Mac silicon
Set up goes something like this:  
- install miniconda3 in `/Applications` (or wherever you like)
- set conda-forge as a channel and set conda priority like this:
```bash
conda config --add channels conda-forge
conda config --set channel_priority strict
conda update --all
```
hint: this can tell you about what pythons you can use with jupyter:
```
jupyter kernelspec list --json
```
I needed to add some way to get jupyter notebooks to see my required python version in miniconda environment via the ipykernal module:  

```bash
conda create --name spin03 python=3
conda activate spin03
/Applications/miniconda3/envs/spin03/bin/python3 -m pip install ipykernel
/Applications/miniconda3/envs/spin03/bin/python3 -m ipykernel install --user --name minienv-3110 --display-name "3.11.0 (minienv)"
conda install uproot
conda install matplotlib
conda install keras
conda install tensorflow
conda install jupyterlab
conda install nb_conda_kernels
conda install scikit-learn
```

# daily setup
```bash
conda activate spin03
cd spinformation
```
# test a simple script
```bash
jupyter notebook notebooks/reg_test_M1_01.ipynb
```

# refs

- [Autoencoder Based Residual Deep Networks for Robust Regression Prediction and Spatiotemporal Estimation](https://arxiv.org/pdf/1812.11262.pdf)  
- [Variational AutoEncoder For Regression: Application to Brain Aging Analysis](https://arxiv.org/pdf/1904.05948.pdf)  
- [Network model tutorials](https://github.com/MorvanZhou/PyTorch-Tutorial)  
- [jupyter and conda pythons](https://www.youtube.com/watch?v=pJ05omgQCMw)  
- [conda channel management](https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-channels.html)  
- [conda cheatsheet](https://medium.com/@skromnitsky/conda-cheatsheet-d3e6a88c32b6)  
- [Mac M1 gpu w tensorflow](https://medium.com/mlearning-ai/install-tensorflow-on-mac-m1-m2-with-gpu-support-c404c6cfb580)
- [install miniconda on mac silicon](https://towardsdatascience.com/how-to-install-miniconda-x86-64-apple-m1-side-by-side-on-mac-book-m1-a476936bfaf0)
- [official miniconda webpage](https://docs.conda.io/en/latest/miniconda.html) 
- [more anaconda + jupyter stuff](https://towardsdatascience.com/how-to-set-up-anaconda-and-jupyter-notebook-the-right-way-de3b7623ea4a)
- [uproot docs (using latest uproot=5)](https://uproot.readthedocs.io/en/latest/)