#!/usr/bin/env python3

import matplotlib.pyplot as plt
import os
import uproot
import pickle
import numpy as np
import matplotlib.pyplot as plt
from keras.models import load_model, model_from_json
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import RobustScaler

os.chdir('./data')

#save_name = "save_model_test1"
save_name = "reg_std_KN_01"
def  get_input_arrays(tree, features_list):
    """function to accesses and orient the input arrays correctly"""
    
    array = np.transpose(tree.arrays(features_list, outputtype=tuple))
    # print(array.shape)
    # print(array[0])
    # print("check for nans:")
    # print(np.where(np.isnan(array) == True))
    return array

os.chdir('../data')
f = uproot.open("KINNW_MC16e_nomSignal_mlpreped_temp/out_proc_0_copy.root")
tree = f["features"]
spin_target = tree.array(b'c_nn')
print("here")
h,b,_=plt.hist(spin_target, bins=25)
spin_weights = 1/(h * 1/np.max(h))
weights_array = np.zeros(len(spin_target))
locs = np.digitize(spin_target, b)
for i in range(len(spin_weights)):
    weights_array[np.where(locs == i+1)[0]] = spin_weights[i]

# Unpickling
#with open(save_name + ".txt", "rb") as fp:
with open(save_name "_feat.txt", "rb") as fp:
    features_list = pickle.load(fp)

    

features_array = get_input_arrays(tree, features_list)
transformer = RobustScaler()
features_array_scaled = transformer.fit_transform(features_array)


#spin_target_scaled = spin_transformer.fit_transform(spin_target.reshape(-1,1))
spin_target_scaled = transformer.fit_transform(spin_target.reshape(-1,1))
spin_target_scaled = (spin_target_scaled - np.min(spin_target_scaled))/(np.max(spin_target_scaled) - np.min(spin_target_scaled))

print_arch_info = True
model = load_model('test_save_model')
print(" -- model loaded.")
print("change to .h5!")
model.load_weights('model_weights.h5')
print(" -- weights loaded.")

#if print_arch_info:
#    model.summary()
    #print(model.get_weights())

# Load model from json
# print("making model from json")
# json_string = model.to_json()
# #print(json_string)
# model_arch = model_from_json(json_string)
# model_arch.summary()


X_train, X_test, y_train, y_test, w_train, w_test = train_test_split(features_array_scaled, spin_target_scaled, weights_array, test_size=0.2)

y_pred = model.predict(X_test)
y_pred_unscaled = y_pred*2 - 1
y_test_unscaled = y_test*2 - 1
fig, ax = plt.subplots(figsize=(10,10))
a = y_pred_unscaled
b = y_test_unscaled
bins=np.histogram(np.hstack((a,b)), bins=10)[1]
h1,b1,_=plt.hist(a, bins, histtype='step', linewidth=2.5, label='Prediction', color='sandybrown')
h2,b2,_=plt.hist(b, bins, histtype='step', linewidth=2.5, label='truth level', color='steelblue')
plt.legend(fontsize=30)
plt.ylim(0,8000)
plt.xlabel(r'cos$\theta_{k}^{+}$', fontsize=30)
plt.ylabel('Aribitrary Unit', fontsize=30)
plt.savefig(save_name + '_fig')
plt.show()
