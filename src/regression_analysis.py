import numpy as np
import matplotlib.pyplot as plt


def guassian_comparison_ratio(data_sig, data_bkg, nBins=20):
    # data_norm = np.random.normal(size=100000)
    # data_sig = 1. - np.random.power(2,size=10000)
    # data_bkg = 1. - np.random.power(6,size=10000)

    # data_sig_std = data_sig
    # data_bkg_std = data_bkg

    # print(uproot.open('/Users/blakeforland/WORKING/SOURCE/dihiggs/data/TARJetImages_hh4W_kt0_5delta0_5_noSD_2019041.root')['tree'].keys())
    # data_sig = uproot.open('/Users/blakeforland/WORKING/SOURCE/dihiggs/data/TARJetImages_hh4W_kt0_5delta0_5_noSD_2019041.root')['tree'].array('jet_pt')
    # data_sig = data_sig[np.logical_not(np.isnan(data_sig))]
    # data_bkg = uproot.open('/Users/blakeforland/WORKING/SOURCE/dihiggs/data/TARJetImages_JZXW_kt0_5delta0_5_noSD_2019041.root')['tree'].array('jet_pt')
    # data_bkg = data_bkg[np.logical_not(np.isnan(data_bkg))]
    data_norm = np.random.normal(size=len(data_bkg))

    data_sig_std = (
        2 * (data_sig - np.min(data_sig)) / (np.max(data_sig) - np.min(data_sig)) - 1
    )
    data_bkg_std = (
        2 * (data_bkg - np.min(data_bkg)) / (np.max(data_bkg) - np.min(data_bkg)) - 1
    )
    data_norm_std = (
        2 * (data_norm - np.min(data_norm)) / (np.max(data_norm) - np.min(data_norm))
        - 1
    )

    # data_sig_std = (data_sig - np.mean(data_sig))/np.std(data_sig)
    # data_bkg_std = (data_bkg - np.mean(data_bkg))/np.std(data_bkg)
    # data_norm_std = (data_norm - np.mean(data_norm))/np.std(data_norm)

    sig_hist, sig_bins, _ = plt.hist(
        data_sig_std, bins=nBins, alpha=0.5, color="red", density=True
    )
    bkg_hist, bkg_bins, _ = plt.hist(
        data_bkg_std, bins=nBins, alpha=0.5, color="blue", density=True
    )
    # plt.hist(data_norm_std,bins=100,alpha=.5,color='green',normed=True)

    # print(sig_hist)
    # print(sig_bins)
    # plt.savefig('dist.png')
    plt.close()

    std_sig_hist, std_sig_bins, _ = plt.hist(
        data_sig_std, bins=nBins, alpha=0.5, color="red", density=True, cumulative=True
    )
    std_bkg_hist, std_bkg_bins, _ = plt.hist(
        data_bkg_std, bins=nBins, alpha=0.5, color="blue", density=True, cumulative=True
    )
    norm_hist, norm_bins, _ = plt.hist(
        data_norm_std,
        bins=nBins,
        alpha=0.5,
        color="green",
        density=True,
        cumulative=True,
    )
    # print(std_bkg_hist)
    # print(norm_hist)
    # print(norm_hist-std_bkg_hist)
    # plt.savefig('std_dist.png')
    plt.close()
    ratio = norm_hist / std_bkg_hist
    """
    #print(ratio)
    #bkg_hist = bkg_hist/len(data_bkg)
    #prototype = [] 
    #for bin,SF in zip(bkg_hist,ratio):
    #    prototype.append( bin*SF)

    #plt.plot(prototype)
    #plt.savefig('prototype.png')
    #plt.close()
    #bkg_gauss_norm=erfinv(std_bkg_bins)
    #print(len(norm_hist))
    #plt.scatter(range(len(ratio)),ratio)
    #plt.savefig('scatter.png')
    #plt.close()
    
    #for i in range(nBins):
    #    bkg_bins = (std_bkg_bins[i+1] - std_bkg_bins[i])/2
    #    sig_bins = (std_sig_bins[i+1] - std_sig_bins[i])/2

    bkg_data_indeces=np.digitize(data_bkg_std, std_bkg_bins[:nBins]) -1 
    sig_data_indeces=np.digitize(data_sig_std, std_sig_bins[:nBins]) -1 

    #final=np.ones(len(bkg_data_indeces))

    weights_bkg = []
    weights_sig = []
    values = []
    #for i in range(len(std_bkg_hist)):
    #    final[np.where(bkg_data_indeces == i)[0]] = ratio[i]
    #print(len(ratio))
    for i in range(len(bkg_data_indeces)):
        weights_bkg.append(ratio[bkg_data_indeces[i]])
        values.append(ratio[bkg_data_indeces[i]]*data_bkg_std[i])
    for i in range(len(sig_data_indeces)):
        weights_sig.append(ratio[sig_data_indeces[i]])

    #print(np.mean(values), np.std(values), np.min(values), np.max(values))

    #print(len(np.where(final=0)[0]))

    #gauss_bkg = np.multiply(data_bkg_std, final)

    #data_sig_std = 2 * (data_sig_std - np.min(data_sig_std))/(np.max(data_sig_std) - np.min(data_sig_std)) - 1
    #data_bkg_std = 2 * (data_bkg_std - np.min(data_bkg_std))/(np.max(data_bkg_std) - np.min(data_bkg_std)) - 1
    
    print(data_bkg_std[:5])
    print(weights_bkg[:5])
    weighted_bkg = np.multiply(data_bkg_std, weights_bkg)
    weighted_sig = np.multiply(data_sig_std, weights_sig)
    final_bkg_stat = np.std(weighted_bkg)/np.mean(weighted_bkg)
    final_sig_stat = np.std(weighted_sig)/np.mean(weighted_sig)
    print(final_bkg_stat)
    print(final_sig_stat)
    print("Error of "+str(np.round(np.sqrt(np.power(final_bkg_stat - final_sig_stat, 2)) * 100, 2))+"%")
    
    fig, ax = plt.subplots(figsize=(16,10))
    h1,b1,_=ax.hist(data_bkg_std,bins=nBins,density=True,weights=weights_bkg,alpha=.2,color='blue')
    h2,b2,_=ax.hist(data_sig_std,bins=nBins,density=True,weights=weights_sig,alpha=.2,color='red')
    #print(np.sqrt(np.power(h1 - h2, 2)))
    print(h1/np.max(h1))
    print(h2/np.max(h2))
    ax.hist(data_bkg_std,bins=nBins,density=True,weights=weights_bkg,alpha=1.,color='blue', histtype='step', linewidth=2.0)
    ax.hist(data_sig_std,bins=nBins,density=True,weights=weights_sig,alpha=1.,color='red', histtype='step', linewidth=2.0)
    plt.title('Regression Analysis', fontsize=24)
    plt.xlabel('Regression Value', fontsize=24)
    plt.ylabel('Arbitrary Unit', fontsize=24)

    plt.savefig('guass_dist.png')
    """
    return ratio


def correct_compare_hist(true_z, test_z_torch, ratio=None):
    hist1, bins1, _ = plt.hist(
        true_z, color="blue", alpha=0.2, bins=20
    )  # , weights=true_z_w)
    plt.close()

    bins2 = bins1
    hist2 = np.zeros(len(hist1))

    # z_reg = np.multiply(test_z_torch.data.numpy().reshape(1000), test_z_w)
    z_reg = test_z_torch.data.numpy().reshape(len(true_z))

    for i in range(len(hist1)):
        hist2[i] = len(
            np.where(z_reg[np.where((z_reg <= bins1[i + 1]))[0]] > bins1[i])[0]
        )  # & (z_reg > bins1[i])[0])]))
        if i == len(hist1):
            hist2[i] = len(np.where(z_reg >= bins1[i])[0])

    b1 = np.zeros(len(hist1))
    w1 = np.zeros(len(hist1))
    for i in range(len(hist1)):
        b1[i] = (bins1[i + 1] + bins1[i]) / 2
        w1[i] = bins1[i + 1] - bins1[i]
    b2 = b1
    w2 = w1

    fig, ax = plt.subplots(figsize=(16, 10))
    plt.cla()
    ax.set_title("Regression Analysis - model 3, Batches", fontsize=35)
    ax.set_xlabel("Regression Values", fontsize=24)
    ax.set_ylabel("Arbitraty Units", fontsize=24)
    hist1 = np.multiply(hist1, ratio)
    hist2 = np.multiply(hist2, ratio)
    plt.bar(
        b1,
        hist1 / np.maximum(np.max(hist1), np.max(hist2)),
        width=w1,
        alpha=0.2,
        color="blue",
    )
    plt.bar(
        b2,
        hist2 / np.maximum(np.max(hist1), np.max(hist2)),
        width=w2,
        alpha=0.2,
        color="red",
    )
    plt.bar(
        b1,
        hist1 / np.maximum(np.max(hist1), np.max(hist2)),
        width=w1,
        alpha=1.0,
        color="none",
        edgecolor="blue",
        linewidth=2.0,
    )
    plt.bar(
        b2,
        hist2 / np.maximum(np.max(hist1), np.max(hist2)),
        width=w2,
        alpha=1.0,
        color="none",
        edgecolor="red",
        linewidth=2.0,
    )

    plt.show()
    return hist1, hist2
