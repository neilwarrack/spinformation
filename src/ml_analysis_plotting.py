import numpy as np
from sklearn.metrics import roc_curve, auc

import matplotlib.pyplot as plt
from matplotlib import gridspec


def plot_loss_acc(history):
    fig, ax1 = plt.subplots(figsize=(12, 9))

    color = "red"
    ax1.set_xlabel("epoch", fontsize=20)
    ax1.set_ylabel("loss", color=color, fontsize=20)
    ax1.set_ylim([0.050, 0.5])
    ax1.plot(history.history["loss"], color=color)
    ax1.plot(history.history["val_loss"], color=color, linestyle="dashed")
    ax1.tick_params(axis="y", labelcolor=color)

    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    color = "blue"
    ax2.set_ylabel(
        "Accuracy", color=color, fontsize=20
    )  # we already handled the x-label with ax1
    ax2.set_ylim([0.7, 1.0])
    ax2.plot(history.history["acc"], color=color)
    ax2.plot(history.history["val_acc"], color=color, linestyle="dashed")
    ax2.tick_params(axis="y", labelcolor=color)

    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.show()


def plot_dnn_features(dnn_data_list, weights_list, nBins=25):

    fig, ax = plt.subplots(int(dnn_data_list[0].shape[1] / 2.0), 3, figsize=(15, 9))

    for i, dnn_data in enumerate(dnn_data_list):

        feature_n = 0

        for j in range(int(dnn_data_list[0].shape[1] / 2.0)):

            for k in range(3):

                if feature_n < dnn_data_list[0].shape[1]:

                    ax[j, k].hist(
                        np.transpose(dnn_data)[feature_n],
                        bins=nBins,
                        weights=weights_list[i],
                        density=True,
                        alpha=0.5,
                    )
                    feature_n += 1


def plot_jet_image(
    content,
    output_fname=None,
    vmin=1e-6,
    vmax=0.2,
    title="",
    name_add="",
    extent=[0, 6, -4, 5],
    saveplot=False,
    norm=None,
):

    fig, ax = plt.subplots(figsize=(7, 6))
    content = np.rot90(content)

    im = ax.imshow(
        content,
        interpolation="nearest",
        extent=extent,
        aspect="auto",
        cmap="cividis",
        norm=norm,
        vmin=vmin,
        vmax=vmax,
    )

    cbar = plt.colorbar(im, fraction=0.05, pad=0.05)
    cbar.set_label(r"# of Tracks", y=0.85)
    plt.xlabel(r"Log (1/$\Delta$)")
    plt.ylabel(r"Log (k$_{t}$)")
    plt.title(" Lund Plane")
    if saveplot:
        plt.savefig("../data/" + name_add + "jet_image.pdf")
    plt.show()


def plot_response(y_test, y_pred, nBins=25):

    sig = np.where(y_test == 1)[0]
    bkg = np.where(y_test == 0)[0]

    fig, ax = plt.subplots(figsize=(12, 9))
    plt.hist(
        y_pred[sig],
        bins=nBins,
        density=True,
        color="blue",
        histtype="step",
        linewidth=1.5,
        label="Signal",
    )
    plt.hist(
        y_pred[bkg],
        bins=nBins,
        density=True,
        color="red",
        histtype="step",
        linewidth=1.5,
        label="Background",
    )
    plt.hist(y_pred[sig], bins=nBins, density=True, color="blue", alpha=0.2)
    plt.hist(y_pred[bkg], bins=nBins, density=True, color="red", alpha=0.2)
    ax.set_yscale("log")
    ax.grid(which="major", linestyle="-", linewidth="0.5", color="black")
    ax.grid(which="minor", linestyle=":", linewidth="0.5", color="black", alpha=0.5)

    plt.legend(loc="upper center", fontsize=20)
    plt.xlabel(r"Network Response", fontsize=20)
    plt.ylabel(r"Unity Density", fontsize=20)

    plt.show()


def plot_ml_roc(y_test, y_pred):

    fpr, tpr, thresholds = roc_curve(y_test, y_pred)

    fig, ax = plt.subplots(figsize=(12, 9))

    lw = 2

    plt.plot(
        fpr,
        tpr,
        color="darkorange",
        lw=lw,
        label="ROC curve (area = %0.2f)" % auc(fpr, tpr),
    )
    plt.plot([0, 1], [0, 1], color="navy", lw=lw, linestyle="--")
    plt.xlim([-0.05, 1.05])
    plt.ylim([0.0, 1.05])
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.title("Receiver operating characteristic example")
    plt.legend(loc="lower right")
    plt.show()


def plot_response_2(y_test, y_pred, nBins=25):

    sig = np.where(y_test == 1)[0]
    bkg = np.where(y_test == 0)[0]

    print(np.max(y_pred[sig]))
    print(np.max(y_pred[bkg]))
    hist, bins, _ = plt.hist(y_pred, bins=nBins)
    plt.close()

    bin_number = np.digitize(y_pred, bins)
    print(bin_number[sig])

    bkg_bars = []
    sig_bars = []

    for i in range(nBins):
        bkg_bars.append(len(np.where(bin_number[bkg] == i)[0]))
        sig_bars.append(len(np.where(bin_number[sig] == i)[0]))

    fig, ax = plt.subplots(figsize=(12, 9))
    # plt.hist(y_pred[sig], bins=nBins, density=True, color="blue",histtype='step',linewidth=1.5, label='Signal')
    # plt.hist(y_pred[bkg], bins=nBins, density=True, color="red",histtype='step',linewidth=1.5, label='Background')
    # plt.hist(y_pred[sig], bins=nBins, density=True, color="blue",alpha=.2)
    # plt.hist(y_pred[bkg], bins=nBins, density=True, color="red",alpha=.2)
    ax.set_yscale("log")
    plt.bar(
        np.arange(nBins) + 1,
        sig_bars,
        alpha=0.3,
        width=1.0,
        linewidth=1.5,
        color="blue",
    )
    plt.bar(
        np.arange(nBins) + 1, bkg_bars, alpha=0.3, width=1.0, linewidth=1.5, color="red"
    )
    ax.grid(which="major", linestyle="-", linewidth="0.5", color="black")
    ax.grid(which="minor", linestyle=":", linewidth="0.5", color="black", alpha=0.5)

    plt.legend(loc="upper center", fontsize=20)
    plt.xlabel(r"Network Response", fontsize=20)
    plt.ylabel(r"Unity Density", fontsize=20)

    plt.show()


def get_eff_rej(y_test, y_pred):

    nbins = 1400

    sig = np.where(y_test == 1)[0]
    bkg = np.where(y_test == 0)[0]

    hist1, b1, _ = plt.hist(y_pred[sig], bins=nbins)
    hist2, b2, _ = plt.hist(y_pred[bkg], bins=nbins)
    plt.close()

    bins1 = (b1[1:] + b1[: len(b1) - 1]) / 2
    # bins2 = (b1[1:] + b1[:len(b1)-1])/2

    sig_eff = np.zeros(len(hist1))
    bkg_rej = np.zeros(len(hist2))

    resp_sum = 0
    wp_res = 0

    for i in range(len(hist1)):
        resp_sum = resp_sum + hist1[-1 * (i + 1)]
        sig_eff[i] = resp_sum / len(sig)
        if (sig_eff[i]) > 0.7 and (wp_res == 0):
            wp_res = bins1[-1 * (i + 1)]
    resp_sum = 0
    for i in range(len(hist2)):
        resp_sum = resp_sum + hist2[-1 * (i + 1)]
        bkg_rej[i] = len(bkg) / resp_sum
    # X_std = (sig_eff - np.min(sig_eff)) / (np.max(sig_eff) - np.min(sig_eff))
    # sig_eff_scaled = X_std * (1.0 - np.min(sig_eff)) + np.min(sig_eff)

    return sig_eff, bkg_rej, bins1, wp_res


def plot_roc(
    sig_eff_list,
    bkg_rej_list,
    bins_list,
    wp_res_list,
    name_list=[""],
    ymax=100000.0,
    name_add="",
):

    fig, ax = plt.subplots(figsize=(12, 9))
    ax.set_yscale("log")

    for i, eff in enumerate(sig_eff_list):
        wp = bkg_rej_list[i][np.where(eff > 0.7)[0][0]]
        plt.plot(
            eff,
            bkg_rej_list[i],
            label=name_list[i]
            + " WP: "
            + str(np.round(wp, 2))
            + " ("
            + str(np.round(wp_res_list[i], 4))
            + ")",
        )

    plt.xlabel(r"Signal Efficiency", fontsize=20)
    plt.ylabel(r"Background Rejection", fontsize=20)
    plt.xlim(0.4, 1.0)
    plt.ylim(0.0, ymax)
    plt.title(" Efficiency vs Rejection", fontsize=20)
    plt.legend(loc="upper right", fontsize=20)
    ax.grid(which="major", linestyle="-", linewidth="0.5", color="black")
    ax.grid(which="minor", linestyle=":", linewidth="0.5", color="black", alpha=0.5)
    plt.savefig("../data/" + name_add + "_roc.png")
    plt.show()
    plt.close()


def plot_significance_2(y_test, y_pred_list, e_test, logY=True):

    n_points = 100
    cuts = np.zeros(n_points)

    mass = e_test[:, 1]

    fig, ax = plt.subplots(figsize=(12, 9))

    if logY is True:
        ax.set_yscale("log")

    for y_pred in y_pred_list:

        significance = []

        bkg = y_pred[
            np.intersect1d(
                np.where(y_test == 0), np.where(mass > 95.0), np.where(mass < 125.0)
            )
        ]
        sig = y_pred[
            np.intersect1d(
                np.where(y_test == 1), np.where(mass > 95.0), np.where(mass < 125.0)
            )
        ]

        for i, c_i in enumerate(np.linspace(0.0, 1.0, n_points)):
            cuts[i] = c_i
            significance.append(
                len(np.where(sig > c_i)[0]) / np.sqrt(len(np.where(bkg > c_i)[0]) + 1.5)
            )

        plt.plot(cuts, significance)

    plt.xlabel(r"Signal Efficiency", fontsize=20)
    plt.ylabel(r"Background Rejection", fontsize=20)
    plt.xlim(0.0, 1.0)
    plt.title(" Efficiency vs Rejection", fontsize=20)
    plt.legend(loc="upper right", fontsize=20)
    ax.grid(which="major", linestyle="-", linewidth="0.5", color="black")
    ax.grid(which="minor", linestyle=":", linewidth="0.5", color="black", alpha=0.5)
    ax.set_yscale("log")
    plt.show()
    # plt.savefig("../data/" + name_add + "_roc.png")
    plt.close()


def plot_significance(
    sig_eff_list, bkg_rej_list, bins_list, wp_res_list, name_list=[""]
):

    fig, ax = plt.subplots(figsize=(12, 9))
    # ax.set_yscale("log")

    significance = []
    for i, eff in enumerate(sig_eff_list):
        significance.append(eff / np.sqrt(1.0 / bkg_rej_list[i] + 1.5))
        plt.plot(bins_list[i], significance[i])

    plt.xlabel(r"Signal Efficiency", fontsize=20)
    plt.ylabel(r"Background Rejection", fontsize=20)
    plt.xlim(0.0, 1.0)
    plt.title(" Efficiency vs Rejection", fontsize=20)
    plt.legend(loc="upper right", fontsize=20)
    ax.grid(which="major", linestyle="-", linewidth="0.5", color="black")
    ax.grid(which="minor", linestyle=":", linewidth="0.5", color="black", alpha=0.5)
    plt.show()
    # plt.savefig("../data/" + name_add + "_roc.png")
    plt.close()


def plot_sub_dist(num, e_test, y_test, y_pred, w_test, cut=0.5):

    print(e_test.shape)

    sig_pre = np.where(y_test == 1)[0]
    bkg_pre = np.where(y_test == 0)[0]

    # mass_range = [np.mean(e_test[:,1][sig_pre])-2*np.std(e_test[:,1][sig_pre]),np.mean(e_test[:,1][sig_pre])+2*np.std(e_test[:,1][sig_pre])]
    mass_range = [np.min(e_test[:, num]), np.max(e_test[:, num])]
    nBins = 25

    # sig = np.intersect1d(np.where(y_test == 1)[0], np.where(y_pred > .5)[0])
    bkg = np.intersect1d(np.where(y_test == 0)[0], np.where(y_pred > cut)[0])

    hist = plt.hist(
        e_test[:, num][bkg],
        weights=w_test[bkg],
        bins=nBins,
        density=True,
        color="blue",
        histtype="step",
        linewidth=1.5,
        range=mass_range,
    )
    plt.close()

    # print(hist[0])
    # print(hist[1])
    points = np.zeros(len(hist[0]))
    errors = np.zeros(len(hist[0]))
    for i in range(len(points)):

        points[i] = (hist[1][i] + hist[1][i + 1]) / 2
        errors[i] = hist[0][i] * 0.7

    fig, ax = plt.subplots(figsize=(12, 9))
    ax.set_yscale("log", nonposy="clip")

    plt.hist(
        e_test[:, num][bkg_pre],
        bins=nBins,
        weights=w_test[bkg_pre],
        density=True,
        color="blue",
        histtype="step",
        linewidth=1.5,
        range=mass_range,
        label="DiJet Mass",
    )
    plt.hist(
        e_test[:, num][bkg_pre],
        bins=nBins,
        density=True,
        color="blue",
        alpha=0.05,
        range=mass_range,
    )

    plt.hist(
        e_test[:, num][sig_pre],
        bins=nBins,
        weights=w_test[sig_pre],
        density=True,
        color="red",
        histtype="step",
        linewidth=1.5,
        range=mass_range,
        label="WW Mass",
    )
    plt.hist(
        e_test[:, num][sig_pre],
        bins=nBins,
        density=True,
        color="red",
        alpha=0.05,
        range=mass_range,
    )

    # plt.scatter(points, hist[0], color='black', label = 'Post Training DiJet')
    plt.hist(
        e_test[:, num][bkg],
        bins=nBins,
        weights=w_test[bkg],
        density=True,
        color="black",
        histtype="step",
        linewidth=1.5,
        range=mass_range,
        label="Post Training DiJet",
    )
    plt.hist(
        e_test[:, num][bkg],
        bins=nBins,
        weights=w_test[bkg],
        density=True,
        color="black",
        alpha=0.05,
        range=mass_range,
    )
    plt.errorbar(points, hist[0], yerr=errors, fmt="o", color="black", alpha=0.4)

    ax.grid(which="major", linestyle="-", linewidth="0.5", color="black")
    ax.grid(which="minor", linestyle=":", linewidth="0.5", color="black", alpha=0.5)
    plt.title("Mass Distribution", fontsize=20)
    plt.xlabel("Dijet Mass", fontsize=20)
    plt.legend(loc="lower right", fontsize=14)
    plt.savefig(str(num) + ".png")
    plt.show()


def plot_mass_dist(
    e_test, y_test, y_pred, cut=0.5, name_add="", pt_min=0, pt_max=600, nBins=25
):

    sig_pre = np.where(y_test == 1)[0]
    bkg_pre = np.where(y_test == 0)[0]

    # mass_range = [np.mean(e_test[:,1][sig_pre])-2*np.std(e_test[:,1][sig_pre]),np.mean(e_test[:,1][sig_pre])+2*np.std(e_test[:,1][sig_pre])]
    mass_range = [pt_min, pt_max]

    # sig = np.intersect1d(np.where(y_test == 1)[0], np.where(y_pred > .5)[0])
    bkg = np.intersect1d(np.where(y_test == 0)[0], np.where(y_pred > cut)[0])

    hist = plt.hist(
        e_test[:, 1][bkg],
        bins=nBins,
        density=True,
        color="blue",
        histtype="step",
        linewidth=1.5,
        range=mass_range,
    )
    plt.close()

    # print(hist[0])
    # print(hist[1])
    points = np.zeros(len(hist[0]))
    errors = np.zeros(len(hist[0]))
    for i in range(len(points)):

        points[i] = (hist[1][i] + hist[1][i + 1]) / 2
        errors[i] = hist[0][i] * 0.7

    # fig = plt.subplots(figsize=(12, 14))
    gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
    # ax.set_yscale('log', nonposy='clip')
    ax1 = plt.subplot(gs[0])

    # plt.subplot(211)
    hist_bkg_pre = ax1.hist(
        e_test[:, 1][bkg_pre],
        bins=nBins,
        density=True,
        color="blue",
        histtype="step",
        linewidth=1.5,
        range=mass_range,
        label="DiJet Mass",
    )
    ax1.hist(
        e_test[:, 1][bkg_pre],
        bins=nBins,
        density=True,
        color="blue",
        alpha=0.05,
        range=mass_range,
    )

    hist_sig_pre = ax1.hist(
        e_test[:, 1][sig_pre],
        bins=nBins,
        density=True,
        color="red",
        histtype="step",
        linewidth=1.5,
        range=mass_range,
        label="WW Mass",
    )
    ax1.hist(
        e_test[:, 1][sig_pre],
        bins=nBins,
        density=True,
        color="red",
        alpha=0.05,
        range=mass_range,
    )

    # plt.scatter(points, hist[0], color='black', label = 'Post Training DiJet')
    hist_bkg_post = ax1.hist(
        e_test[:, 1][bkg],
        bins=nBins,
        density=True,
        color="black",
        histtype="step",
        linewidth=1.5,
        range=mass_range,
        label="Post Training DiJet",
    )
    ax1.hist(
        e_test[:, 1][bkg],
        bins=nBins,
        density=True,
        color="black",
        alpha=0.05,
        range=mass_range,
    )
    # ax1.errorbar(points, hist[0], yerr=errors, fmt='o', color='black', alpha=.4)

    ax1.grid(which="major", linestyle="-", linewidth="0.5", color="black")
    ax1.grid(which="minor", linestyle=":", linewidth="0.5", color="black", alpha=0.5)
    plt.title("Mass Distribution", fontsize=20)
    # plt.xlabel('Dijet Mass', fontsize=20)
    plt.ylabel("Response", fontsize=20)
    plt.legend(loc="upper right", fontsize=14)
    # plt.savefig('../data/mass_compare'+name_add+'.png')
    # plt.show()

    # fig, ax = plt.subplots(figsize=(12, 9))

    sig_vs_bkg = (hist_sig_pre[0] - np.mean(hist_sig_pre[0])) / np.std(
        hist_sig_pre[0]
    ) - (hist_bkg_pre[0] - np.mean(hist_bkg_pre[0])) / np.std(hist_bkg_pre[0])
    # hist_comp_bkg = (hist_bkg_post[0] - np.mean(hist_bkg_post[0])) / np.std(
    #    hist_bkg_post[0]
    # ) - (hist_bkg_pre[0] - np.mean(hist_bkg_pre[0])) / np.std(hist_bkg_pre[0])
    # hist_sig = (hist_sig_pre[0] - np.mean(hist_sig_pre[0])) / np.std(hist_sig_pre[0])
    # hist_comp_bkg = hist_comp_bkg - sig_vs_bkg
    hist_comp_sig = (hist_bkg_post[0] - np.mean(hist_bkg_post[0])) / np.std(
        hist_bkg_post[0]
    ) - (hist_sig_pre[0] - np.mean(hist_sig_pre[0])) / np.std(hist_sig_pre[0])
    # hist_comp_sig = hist_comp_sig - sig_vs_bkg
    """
    plt.bar(points, (-hist_sig)/np.max(np.abs(hist_sig)),
            width=np.max(mass_range)/nBins,
            alpha=1.0,
            linewidth=2,
            fill=False,
            edgecolor='green',
            color='green',
            label = 'Singal')
    plt.bar(points, (sig_vs_bkg)/np.max(np.abs(sig_vs_bkg)),
            width=np.max(mass_range)/nBins,
            alpha=.2,
            linewidth=3,
            edgecolor='black',
            hatch='/',
            color='black',
            label = 'BKG Baseline: '+str(np.round(100.-np.sum(np.abs(sig_vs_bkg)),3))+'% SIG Like')
    plt.bar(points, (-1*hist_comp_sig)/np.max(np.abs(sig_vs_bkg)),
            width=np.max(mass_range)/nBins,
            alpha=.5, color='red',
            linewidth=1,
            edgecolor='red',
            label = str(np.round(100.-np.sum(np.abs(hist_comp_sig)),3))+'% SIG Like')
    """
    difference = np.zeros(len(sig_vs_bkg))

    for i in range(len(difference)):
        if np.sign(sig_vs_bkg[i]) == np.sign(hist_comp_sig[i]):
            difference[i] = sig_vs_bkg[i] - hist_comp_sig[i]  # - sig_vs_bkg[i]
            # if difference[i] == np.sign((-1*sig_vs_bkg[i])):
            #    difference[i] = (sig_vs_bkg[i]) + difference[i]
            # else:
            #    difference[i] = (sig_vs_bkg[i]) - difference[i]

        else:
            difference[i] = sig_vs_bkg[i] + hist_comp_sig[i]  # + sig_vs_bkg[i]
            # if difference[i] != np.sign((-1*sig_vs_bkg[i])):
            #    difference[i] = (sig_vs_bkg[i]) + difference[i]
            # else:
            #    difference[i] = (sig_vs_bkg[i]) - difference[i]

    mass_score = np.round(
        1.0
        + (np.sum(np.abs(hist_comp_sig)) - np.sum(np.abs(sig_vs_bkg)))
        / (np.sum(np.abs(sig_vs_bkg))),
        3,
    )
    """
    plt.bar(points, -1*difference/np.max(np.abs(sig_vs_bkg)),
            width=np.max(mass_range)/nBins,
            alpha=.5,
            linewidth=3,
            edgecolor='blue',
            label = 'Difference; Score '+str(mass_score))
    """
    ax2 = plt.subplot(gs[1], sharex=ax1)
    ax2.scatter(
        points,
        -1 * difference / np.max(np.abs(sig_vs_bkg)),
        label="Inegrated MS: " + str(mass_score),
    )
    # ax2.plot(([0,0], [0,1]), linestyle='-', linewidth='3', color='black')
    plt.legend(loc="lower right")

    ax2.grid(which="major", linestyle="-", linewidth="0.5", color="black")
    ax2.grid(which="minor", linestyle=":", linewidth="0.5", color="black", alpha=0.5)
    # plt.title('Rough Shape comparison', fontsize=20)
    plt.ylabel("Shape Difference", fontsize=20)
    plt.xlabel("Dijet Mass", fontsize=20)
    plt.legend(loc="lower right", fontsize=14)
    # plt.axis('equal')
    plt.ylim((-1, 1))
    plt.savefig("../data/shape_compare" + name_add + ".png")
    plt.show()

    return mass_score


def save_to_csv(file, y_pred, extras):
    np.savetxt(
        file,
        np.transpose(
            np.reshape(
                np.concatenate(
                    (
                        np.reshape(y_pred, (len(y_pred))),
                        np.transpose(extras)[0],
                        np.transpose(extras)[1],
                    )
                ),
                (3, len(y_pred)),
            )
        ),
        delimiter=",",
        header="response,jet_pt,jet_m",
    )


def guassian_comparison_ratio(array1, array2, nBins):
    # data_norm = np.random.normal(size=100000)
    # data_sig = 1. - np.random.power(2,size=10000)
    # data_bkg = 1. - np.random.power(6,size=10000)

    # data_sig_std = data_sig
    # data_bkg_std = data_bkg

    # print(uproot.open('/Users/blakeforland/WORKING/SOURCE/dihiggs/data/TARJetImages_hh4W_kt0_5delta0_5_noSD_2019041.root')['tree'].keys())
    # data_sig = uproot.open('/Users/blakeforland/WORKING/SOURCE/dihiggs/data/TARJetImages_hh4W_kt0_5delta0_5_noSD_2019041.root')['tree'].array('jet_pt')
    # data_sig = data_sig[np.logical_not(np.isnan(data_sig))]
    # data_bkg = uproot.open('/Users/blakeforland/WORKING/SOURCE/dihiggs/data/TARJetImages_JZXW_kt0_5delta0_5_noSD_2019041.root')['tree'].array('jet_pt')
    # data_bkg = data_bkg[np.logical_not(np.isnan(data_bkg))]

    array_norm_std = np.random.normal(size=np.max((len(array1), len(array2))))
    array1_std = array1
    array2_std = array2
    """
    array_norm = np.random.normal(size=np.max((len(array1),len(array2))))
    array1_std = (
        2 * (array1 - np.min(array1)) / (np.max(array1) - np.min(array1)) - 1
    )
    array2_std = (
        2 * (array2 - np.min(array2)) / (np.max(array2) - np.min(array2)) - 1
    )
    array_norm_std = (
        2 * (array_norm - np.min(array_norm)) / (np.max(array_norm) - np.min(array_norm))
        - 1
    )
    """
    # data_sig_std = (data_sig - np.mean(data_sig))/np.std(data_sig)
    # data_bkg_std = (data_bkg - np.mean(data_bkg))/np.std(data_bkg)
    # data_norm_std = (data_norm - np.mean(data_norm))/np.std(data_norm)

    # sig_hist, sig_bins, _ = plt.hist(
    #    data_sig_std, bins=nBins, alpha=0.5, color="red", density=True
    # )
    # bkg_hist, bkg_bins, _ = plt.hist(
    #    data_bkg_std, bins=nBins, alpha=0.5, color="blue", density=True
    # )
    # plt.hist(data_norm_std,bins=100,alpha=.5,color='green',normed=True)

    # print(sig_hist)
    # print(sig_bins)
    # plt.savefig('dist.png')
    # plt.close()

    hist_std1, bins_std1, _ = plt.hist(
        array1_std, bins=nBins, alpha=0.5, color="red", density=True, cumulative=True
    )
    hist_std2, bins_std2, _ = plt.hist(
        array2_std, bins=nBins, alpha=0.5, color="blue", density=True, cumulative=True
    )

    hist_norm_std, bins_norm_std, _ = plt.hist(
        array_norm_std,
        bins=nBins,
        alpha=0.5,
        color="green",
        density=True,
        cumulative=True,
    )
    plt.close()
    # print(std_bkg_hist)
    # print(norm_hist)
    # print(norm_hist-std_bkg_hist)
    # plt.savefig('std_dist.png')

    if len(array1_std) <= len(array2_std):
        ratio = hist_norm_std / hist_std2
    else:

        ratio = hist_norm_std / hist_std1
        plt.close()

    return ratio


def get_hists(array1, array2, nBins):

    """
    array1 = (
        2 * (array1 - np.min(array1)) / (np.max(array1) - np.min(array1)) - 1
    )
    array2 = (
        2 * (array2 - np.min(array2)) / (np.max(array2) - np.min(array2)) - 1
    )
    """

    hist1, bins1, _ = plt.hist(
        array2, color="blue", alpha=0.2, bins=nBins
    )  # , weights=true_z_w)
    plt.close()
    bins2 = bins1
    hist2 = np.zeros(len(hist1))

    # z_reg = np.multiply(test_z_torch.data.numpy().reshape(1000), test_z_w)
    # z_reg = test_z_torch.data.numpy().reshape(len(true_z))
    # z_reg = test_z_torch

    for i in range(len(hist1)):
        hist2[i] = len(
            np.where(array1[np.where((array1 <= bins1[i + 1]))[0]] > bins1[i])[0]
        )  # & (z_reg > bins1[i])[0])]))
        if i == len(hist1):
            hist2[i] = len(np.where(array1 >= bins1[i])[0])
    return hist1, bins1, hist2, bins2


def get_weights(array, bins, ratio):

    weights = np.zeros(len(array))
    bins = np.digitize(array, bins)

    for i in range(len(ratio)):
        weights[np.where(bins == i)] = ratio[i]

    return weights


def correct_compare_hist(
    array1, array2, nBins=40, normed=False, gauss=False, logY=False
):

    array1 = removeNan(array1)
    array2 = removeNan(array2)

    if gauss is True:
        ratio = guassian_comparison_ratio(array1, array2, nBins)
    else:
        ratio = np.ones(nBins)

    if len(array1) <= len(array2):
        hist1, bins1, hist2, bins2 = get_hists(array2, array1, nBins)

    else:
        hist1, bins1, hist2, bins2 = get_hists(array1, array2, nBins)

    b1 = np.zeros(len(hist1))
    w1 = np.zeros(len(hist1))
    for i in range(len(hist1)):
        b1[i] = (bins1[i + 1] + bins1[i]) / 2
        w1[i] = bins1[i + 1] - bins1[i]
    b2 = b1
    w2 = w1

    fig, ax = plt.subplots(figsize=(16, 10))
    plt.cla()
    if logY is True:
        ax.set_yscale("log")
    ax.set_title("Regression Analysis - model 3, Batches", fontsize=35)
    ax.set_xlabel("Jet Mass", fontsize=24)
    ax.set_ylabel("Arbitraty Units", fontsize=24)
    hist1 = np.multiply(hist1, ratio)
    hist2 = np.multiply(hist2, ratio)

    weights1 = get_weights(array1, bins1, ratio)
    weights2 = get_weights(array2, bins2, ratio)

    if normed is False:
        scale1 = np.maximum(np.max(hist1), np.max(hist2))
        scale2 = scale1
    else:
        scale1 = np.max(hist1)
        scale2 = np.max(hist2)

    plt.bar(b1, hist1 / scale1, width=w1, alpha=0.2, color="blue", label="Truth")
    plt.bar(b2, hist2 / scale2, width=w2, alpha=0.2, color="red", label="Regression")
    plt.bar(
        b1,
        hist1 / scale1,
        width=w1,
        alpha=1.0,
        color="none",
        edgecolor="blue",
        linewidth=2.0,
    )
    plt.bar(
        b2,
        hist2 / scale2,
        width=w2,
        alpha=1.0,
        color="none",
        edgecolor="red",
        linewidth=2.0,
    )
    plt.legend()
    plt.show()
    return weights1, weights2


def removeNan(array):
    if len(np.where(np.isnan(array) is True)) == 0:
        pass
    return array[np.logical_not(np.isnan(array))]


def plot_AMS1(pred, Y):

    x00 = np.sort(pred[(Y == 0)].flatten())
    # x01 = np.sort(pred[(Y == 0)].flatten())
    x10 = np.sort(pred[(Y == 1)].flatten())

    n_points = 100
    AMS1 = np.zeros(n_points)
    all_ns = np.zeros(n_points)
    all_nb = np.zeros(n_points)
    all_nb1 = np.zeros(n_points)
    all_sigb = np.zeros(n_points)

    cuts = np.zeros(n_points)
    sig_eff = np.zeros(n_points)
    ns_tot = x10.shape[0]

    for i, c_i in enumerate(np.linspace(0.0, 0.99, n_points)):
        cuts[i] = c_i

        ns = (100 / x10.size) * np.count_nonzero(x10 > c_i)
        nb = (1000 / x00.size) * np.count_nonzero(x00 > c_i)
        # nb1 = (1000 / x01.size) * np.count_nonzero(x01 > c_i)
        # sig_b = 1.0 * np.abs(nb - nb1)
        sig_b = 1.0
        # plot_ams(y_test, dnn_y_pred)(ns, nb, sig_b)
        b0 = 0.5 * (
            nb
            - sig_b ** 2
            + ((nb - sig_b ** 2) ** 2 + 4 * (ns + nb) * (sig_b ** 2)) ** 0.5
        )
        AMS1[i] = (
            2.0 * ((ns + nb) * np.log((ns + nb) / b0) - ns - nb + b0)
            + ((nb - b0) / sig_b) ** 2
        ) ** 0.5
        # print(b0)
        # print(AMS1[i])
        all_ns[i] = ns
        all_nb[i] = nb
        all_nb1[i] = nb
        all_sigb[i] = sig_b

        sig_eff[i] = (1.0 * ns) / ns_tot

    return cuts, AMS1


def plot_ams(y_test, y_pred):
    c = ["r"]
    ams = []
    cuts, a = plot_AMS1(y_pred, y_test)
    ams.append(a)
    mu = np.mean(ams, axis=0)
    std = np.std(ams, axis=0)
    fig, ax = plt.subplots(figsize=(16, 10))
    plt.cla()
    ax.set_title("Regression Analysis - model 3, Batches", fontsize=35)
    ax.set_xlabel("Response", fontsize=24)
    ax.set_ylabel("Significance", fontsize=24)
    ax.set_ylim(0, 15)
    ax.set_xlim(0, 1)
    ax.grid(which="major", linestyle="-", linewidth="0.5", color="black")
    ax.grid(which="minor", linestyle=":", linewidth="0.5", color="black")
    plt.legend(loc="upper right", fontsize=20)
    plt.plot(cuts, ams[0])  # , label=r"$\lambda=%d|Z=0$" % lam, c=c)
    plt.fill_between(cuts, mu + std, mu - std, color=c, alpha=0.1)
    plt.show()
